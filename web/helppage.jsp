<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Movie Page</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">

        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">

        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <!-- Page scripts -->
        <link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
    </head>
    <body>
        <jsp:useBean class="beans.Movie" id="curMovie" scope="session" ></jsp:useBean>
        <jsp:include page="/header.jsp"/> 
        
        <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Help Page</h2>
                </div>
            </div>
        </div>
    </div>

    <section class="slice white">
        <div class="wp-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <p>Gingko is a collaborative project created for CSE 308 at stony brook university</p>
                        <ul class="list-check">
                            <li><i class="fa fa-check"></i> Having issues? Contact us:</li>
                            <li><i class="fa fa-check"></i> Ritvik Handa: ritvik.handa@stonybrook.edu</li>
                            <li><i class="fa fa-check"></i> Ravikiran Nageli: ravikiran.nageli@stonybrook.edu</li>
                        </ul>
                        <p>Thank you for visiting our website! We hope you enjoy your stay!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

         <!-- Essentials -->
        <script src="js/modernizr.custom.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script src="js/jquery.metadata.js"></script>
        <script src="js/jquery.hoverup.js"></script>
        <script src="js/jquery.hoverdir.js"></script>
        <script src="js/jquery.stellar.js"></script>
        <!-- Boomerang mobile nav - Optional  -->
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>
        <!-- Forms -->
        <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script>
        <script src="assets/ui-kit/js/cusel.min.js"></script>
        <script src="assets/sky-forms/js/jquery.form.min.js"></script>
        <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
        <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
        <script src="assets/sky-forms/js/jquery.modal.js"></script>
        <!-- Assets -->
        <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
        <script src="assets/mixitup/jquery.mixitup.js"></script>
        <script src="assets/mixitup/jquery.mixitup.init.js"></script>
        <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script src="assets/waypoints/waypoints.min.js"></script>
        <script src="assets/milestone-counter/jquery.countTo.js"></script>
        <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
        <script src="assets/social-buttons/js/rrssb.min.js"></script>
        <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
        <script src="assets/owl-carousel/owl.carousel.js"></script>
        <script src="assets/bootstrap/js/tooltip.js"></script>
        <script src="assets/bootstrap/js/popover.js"></script>
        <!-- Sripts for individual pages, depending on what plug-ins are used -->
        <script src="assets/layerslider/js/greensock.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
        <!-- Initializing the slider -->
        <script>
            jQuery("#layerslider").layerSlider({
                pauseOnHover: true,
                autoPlayVideos: false,
                skinsPath: 'assets/layerslider/skins/',
                responsive: false,
                responsiveUnder: 1280,
                layersContainer: 1280,
                skin: 'borderlessdark3d',
                hoverPrevNext: true,
            });
        </script>
        <!-- Boomerang App JS -->
        <script src="js/wp.app.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <!-- Temp -- You can remove this once you started to work on your project -->
        <script src="js/jquery.cookie.js"></script>
        <script src="js/wp.switcher.js"></script>
        <script type="text/javascript" src="js/wp.ga.js"></script>
    </body> 
</html>