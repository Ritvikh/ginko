/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Movie;
import beans.Theater;
import beans.TheaterTiming;
import controller.webdata.MovieTiming;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author RAVI
 */
public class TheaterPage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String theaterId = request.getParameter("theaterID");
        String name = request.getParameter("name");
        String check=request.getParameter("near");
        name = name.replace(" ", "");
        String url = name.concat("_");
        url = url.concat(theaterId);
        MovieTiming mt = new MovieTiming();
        List<Theater> theater=null;
        
        if(check.equals("check")){
        theater = (List<Theater>) session.getAttribute("nearTheaters");
        session.setAttribute("near","check");
        }else{
            theater = (List<Theater>) session.getAttribute("searchTheater");
             session.setAttribute("near","uncheck");
        }
        String date=request.getParameter("date");
        if(date==null){
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
            Calendar cal=Calendar.getInstance();
            date = simpleDateFormat1.format(cal.getTime());
            session.setAttribute("curDate", date);
        }
        if (!theater.isEmpty() && theaterId != null) {
            for (Theater t : theater) {
                if (t.getId().equals(theaterId)) {
                    List<TheaterTiming> times = mt.getTheaters(url,date);
                    session.setAttribute("curTimings", times);
                    session.setAttribute("curTheater", t);
                    session.setAttribute("curDate", date);
                    request.getRequestDispatcher("/theater.jsp").include(request, response);
                }
            }
        } else {

            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet MoviePage</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Servlet MoviePage at " + request.getContextPath() + "</h1>");
                out.println("<p>Hey the movie page you have clicked isn't available</p>");
                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
