/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Comment;
import beans.Movie;
import beans.Theater;
import beans.Userdata;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author RAVI
 */
public class CommentControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Movie m;
        Theater t;
        CrudOperator crd = new CrudOperator();
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Userdata user = (Userdata) session.getAttribute("userData");
        if (user == null) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Please sign in to comment');");
            out.println("location='moviepage.jsp';");
            out.println("</script>");
        } else {
            int id = 0;
            Boolean check = true;
            while (check) {
                id = (int) Math.round(Math.random() * 100000);
                Comment comm = crd.getComment(id);
                if (comm == null) {
                    check = false;
                }
            }
            Comment comment = new Comment();
            comment.setCommentId(id);
            comment.setContent(request.getParameter("addComment"));
            comment.setDate(new Date());
            comment.setUserId(user);
            if (request.getParameter("movie").equals("true")) {
                m = (Movie) session.getAttribute("curMovie");
                comment.setMovieId(m);
                m.getCommentCollection();
                m=crd.getMovie(m.getId());
                session.setAttribute("curMovie", m);
                
            } else {
                t = (Theater) session.getAttribute("curTheater");
                comment.setTheaterId(t);
            }

            //comment.setCommentId(4);
            Boolean b = false;
            b = crd.addComment(comment);
            if (b) {
                try {
                    out.println("<li class=\"comment\">");
                    out.println("<div class=\"comment-body boxed\">");
                    out.println("<div class=\"comment-avatar\">");
                    out.println("<div class=\"avatar\"><img src=\"images/temp/fb-dp.jpg\" alt=\"\"></div>");
                    out.println("</div>" + "<div class=\"comment-text\">");
                    out.println("<div class=\"comment-author clearfix\">");
                    out.println("<a href=\"#\" class=\"link-author\">" + user.getUsername() + "</a>" + "</div>");
                    out.println("<div class=\"comment-entry\">" + request.getParameter("addComment"));
                    out.println("</div>");
                    out.println("</div>");
                    out.println("</div>");
                    out.println("</li>");
                } catch (Exception e) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Comment error');");
                    out.println("location='moviepage.jsp';");
                    out.println("</script>");
                }
            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Comment could not be added');");
                out.println("location='moviepage.jsp';");
                out.println("</script>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
