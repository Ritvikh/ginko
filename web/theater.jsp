<%-- 
    Document   : theater
    Created on : May 17, 2015, 10:14:44 AM
    Author     : RAVI
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Theater Page</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">

        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">

        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <!-- Page scripts -->
        <link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
    </head>
    <body>
        <jsp:include page="/header.jsp"/>
        <jsp:useBean id="curTheater" class="beans.Theater" scope="session"></jsp:useBean>
        <div class="pg-opt">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>${curTheater.getName()}</h2>
                    </div>
                </div>
            </div>
        </div>
        <section class="slice bg-white">
            <div class="wp-section">
                <div class="container">
                    <div class="row ">  
                        <div class="col-md-6">
                            <img src="${curTheater.getImgUrl()}" width="200" height="100" class="img-responsive" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-7">
                                    <h4 class="title">Information <br>
                                    </h4>
                                </div>
                                <div class="col-md-5">
                                    <div class="social-media social-media-sm text-right">
                                        <a href="#" title="Facebook"><i class="fa fa-facebook facebook"></i></a>
                                        <a href="#" title="Google +"><i class="fa fa-google-plus google"></i></a>
                                        <a href="#" title="Twitter"><i class="fa fa-twitter twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                            <h3 class="title-lg"></h3>
                            <p>Address: ${curTheater.getAddress()} ${curTheater.getCity()} </p>
                            <p>${curTheater.getState()} ${curTheater.getZip()} </p>
                            <p>Phone Number: ${curTheater.getPhoneNumber()}</p>
                            <p> Amenities ${curTheater.getAmenities()}
                            </p>
                            <p> Bo Info ${curTheater.getBoInfo()}</p>
                            <p> Age Policy ${curTheater.getAgePolicy()}</p>
                        </div>
                        </br>
                        <h2> Now Showing: </h2>
                        <div class="table-responsive">
                            <table class="table" spacing="1">
                                <%
                                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
                                    Calendar cal = Calendar.getInstance();
                                    String date = simpleDateFormat1.format(cal.getTime());
                                %>
                                <tbody>
                                    <tr>
                                        <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td>
                                        <%cal.add(Calendar.DATE, 1);
                                        date = simpleDateFormat1.format(cal.getTime());%>
                                        <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td> 
                                        <%cal.add(Calendar.DATE, 1);
                                        date = simpleDateFormat1.format(cal.getTime());%>
                                        <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td> 
                                        <%cal.add(Calendar.DATE, 1);
                                        date = simpleDateFormat1.format(cal.getTime());%>
                                        <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td> 
                                        <%cal.add(Calendar.DATE, 1);
                                        date = simpleDateFormat1.format(cal.getTime());%>
                                        <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td>
                                        <%cal.add(Calendar.DATE, 1);
                                        date = simpleDateFormat1.format(cal.getTime());%>
                                        <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td>
                                          <%cal.add(Calendar.DATE, 1);
                                        date = simpleDateFormat1.format(cal.getTime());%>
                                         <td><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${curTheater.getId()}&name=${curTheater.getName()}&date=<%=date%>&amp;near=${near}" class="btn btn-base"><%=date%></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                                    <p> Showing times for: ${sessionScope.curDate}</p>
                        <section class="slice bg-white bb">
                            <div class="wp-section">
                                <div class="container">
                                    <c:forEach items="${sessionScope.curTimings}" var="mov" >
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="wp-block property list">
                                                        <div class="wp-block-title">
                                                            <h3><a href="http://localhost:8080/Ginko/MoviePage?movieID=${mov.getMovie().getId()}" >${mov.getMovie().getTitle()}</a></h3>
                                                        </div>
                                                        <div class="wp-block-body">
                                                            <div class="wp-block-img">
                                                                <a href="#">
                                                                    <img src="${mov.getMovie().getThumbnail()}" 
                                                                         alt="" 
                                                                         style="width:100%;max-width:100px;height:auto;">
                                                                </a>
                                                            </div>
                                                            <div class="wp-block-content clearfix">
                                                                <h5 class="title">Timings</h5>
                                                                <p><br></p>
                                                                <table class="table table-bordered table-hover table-striped">
                                                                    <tbody>
                                                                        <tr>
                                                                    <c:forEach items="${mov.getTimes()}" var="time" varStatus="i">
                                                                            <td><a class="title" href="http://localhost:8080/Ginko/BookTicket?time=${time}&movie=${mov.getMovie().getTitle()}">${time}</a></td>
                                                                        </c:forEach>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div></c:forEach>
                                </div>
                            </div>
                        </section> 
                                                        </br>
                        
                        <div class="col"> 
                            <div class="comment-list clearfix" id="comments">
                                <h2 class="comment-count">Comments:</h2>
                                <ol>
                                    <li class="comment">
                                        <div class="comment-body boxed">
                                            <div class="comment-avatar">
                                                <div class="avatar"><img src="images/temp/avatar1.png" alt=""></div>
                                            </div>
                                            <div class="comment-text">
                                                <div class="comment-author clearfix">
                                                    <a href="#" class="link-author">Brad Pit</a>
                                                </div>
                                                <div class="comment-entry">
                                                    William Bradley "Brad" Pitt is an American actor and film producer. Pitt has received four Academy Award nominations and five Golden Globe.
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="col">
                            <div class="section-title-wr">
                                <h3 class="section-title left"><span>Add A Comment</span></h3>
                            </div>
                            <form class="form-light" role="form">
                                <p>
                                    Improve this conversation by adding your comment :)
                                </p>
                                <div class="form-group form-group-lg">
                                    <label class="">Comment</label>
                                    <textarea class="form-control" rows="5" placeholder="Type you comment"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <button class="btn btn-lg btn-alt btn-icon btn-icon-right btn-comment pull-right">
                                            <span>Send comment</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div> 
                </div>
            </div>

        </section>



        <!-- Essentials -->
        <script src="js/modernizr.custom.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script src="js/jquery.metadata.js"></script>
        <script src="js/jquery.hoverup.js"></script>
        <script src="js/jquery.hoverdir.js"></script>
        <script src="js/jquery.stellar.js"></script>
        <!-- Boomerang mobile nav - Optional  -->
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>
        <!-- Forms -->
        <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script>
        <script src="assets/ui-kit/js/cusel.min.js"></script>
        <script src="assets/sky-forms/js/jquery.form.min.js"></script>
        <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
        <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
        <script src="assets/sky-forms/js/jquery.modal.js"></script>
        <!-- Assets -->
        <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
        <script src="assets/mixitup/jquery.mixitup.js"></script>
        <script src="assets/mixitup/jquery.mixitup.init.js"></script>
        <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script src="assets/waypoints/waypoints.min.js"></script>
        <script src="assets/milestone-counter/jquery.countTo.js"></script>
        <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
        <script src="assets/social-buttons/js/rrssb.min.js"></script>
        <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
        <script src="assets/owl-carousel/owl.carousel.js"></script>
        <script src="assets/bootstrap/js/tooltip.js"></script>
        <script src="assets/bootstrap/js/popover.js"></script>
        <!-- Sripts for individual pages, depending on what plug-ins are used -->
        <script src="assets/layerslider/js/greensock.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
        <!-- Initializing the slider -->
        <script>
            jQuery("#layerslider").layerSlider({
                pauseOnHover: true,
                autoPlayVideos: false,
                skinsPath: 'assets/layerslider/skins/',
                responsive: false,
                responsiveUnder: 1280,
                layersContainer: 1280,
                skin: 'borderlessdark3d',
                hoverPrevNext: true,
            });
        </script>
        <!-- Boomerang App JS -->
        <script src="js/wp.app.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <!-- Temp -- You can remove this once you started to work on your project -->
        <script src="js/jquery.cookie.js"></script>
        <script src="js/wp.switcher.js"></script>
        <script type="text/javascript" src="js/wp.ga.js"></script>
    </body>
</html>
