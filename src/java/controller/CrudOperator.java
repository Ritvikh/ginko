/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Comment;
import beans.Movie;
import beans.Subscription;
import beans.Theater;
import beans.Userdata;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.json.simple.JSONObject;

public class CrudOperator {

    Theater t = new Theater();

    public CrudOperator() {

    }

    public List getMovies(String searchString) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        String sqlText
                = "select * "
                + "from movie "
                + "where title like ?";
        //+ "and TEAM.TEAMNAME='Los Angeles Dodgers'";
        Query q;
        List<Movie> movieList;
        q = em.createNativeQuery(sqlText, Movie.class);
        q.setParameter(1, "%" + searchString + "%");
        movieList = q.getResultList();
        for (Movie p : movieList) {
            System.out.println(p.toString());
        }
        return movieList;
    }

    public List getTheaters(String search) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        String sqlText
                = "select * "
                + "from theater "
                + "where name like ?";
        //+ "and TEAM.TEAMNAME='Los Angeles Dodgers'";
        Query q;
        List<Theater> theaterList;
        q = em.createNativeQuery(sqlText, Theater.class);
        q.setParameter(1, "%" + search + "%");
        theaterList = q.getResultList();
        for (Theater p : theaterList) {
            System.out.println(p.toString());
        }
        return theaterList;
    }

    public Date returnDate(String s) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(s);
            //date=format.format(date);
        } catch (Exception ex) {
            System.out.println("Cought in returnDate");
            ex.printStackTrace();
        }
        return date;
    }
    
    public Userdata changePassword(String email,String password){
        Userdata user=this.getUser(email);
        user.setPassword(password);
        try{
            this.merge(user);
            return user;
        }catch(Exception e){
            System.out.println("caught while changing password"+e);
            return user;
        }
    }

    public String formatImage(String imgURL) {
        String original = imgURL;
        try {
            imgURL = imgURL.replace("resizing", "content6");
            int index = imgURL.indexOf("movie");
            StringBuilder cut = new StringBuilder(imgURL);
            cut.delete(29, index);
            imgURL = cut.toString();
        } catch (Exception e) {
            System.out.println("pic not set ");
            return original;
        }
        return imgURL;
    }

    public Userdata getUser(String name) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Userdata user = em.find(Userdata.class, name);
        return user;
    }
    
    public Comment getComment(int id){
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Comment comment = em.find(Comment.class, id);
        return comment;
    }

    public Movie getMovie(int id) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Movie m = em.find(Movie.class, id);
        return m;
    }
    
    public Theater getTheater(String id) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Theater m = em.find(Theater.class, id);
        return m;
    }

    public void deleteMovie(int id) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Movie m = em.find(Movie.class, id);
        try {
            EntityTransaction et = em.getTransaction();
            et.begin();
            em.remove(m);
            em.flush();
            et.commit();
        } catch (Exception e) {
            System.out.println("Cought while deleting a movie" + e);
            em.getTransaction().rollback();
        }
    }
    
    public void deleteComment(int id) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Comment c = em.find(Comment.class, id);
        try {
            EntityTransaction et = em.getTransaction();
            et.begin();
            em.remove(c);
            em.flush();
            et.commit();
        } catch (Exception e) {
            System.out.println("Cought while deleting a comment" + e);
            em.getTransaction().rollback();
        }
    }

    public void updatePassword(String pw, Userdata user) {
        user.setPassword(pw);
        this.merge(user);
    }

    public void deleteUser(String email) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Userdata u = em.find(Userdata.class, email);
        try {
            EntityTransaction et = em.getTransaction();
            et.begin();
            em.remove(u);
            em.flush();
            et.commit();
        } catch (Exception e) {
            System.out.println("Cought while deleting a movie" + e);
            em.getTransaction().rollback();
        }
    }

    public Boolean addUser(Userdata user) {
        try {
            this.persist(user);
            return true;
        } catch (Exception e) {
            System.out.println("Cought while adding a user" + e);
            return false;
        }
    }
    
    public Boolean addMovie(Movie m) {
        try {
            this.persist(m);
            return true;
        } catch (Exception e) {
            System.out.println("Cought while adding a movie by admin" + e);
            return false;
        }
    }
    
    public Boolean addSubscriber(Subscription sc){
        try {
            this.persist(sc);
            return true;
        } catch (Exception e) {
            System.out.println("Cought while adding a movie by admin" + e);
            return false;
        }
    }
    
    public Boolean updateMovie(Movie m) {
        try {
            this.merge(m);
            return true;
        } catch (Exception e) {
            System.out.println("Cought while adding a user" + e);
            return false;
        }
    }
    
    public Boolean updateUser(Userdata user) {
        try {
            this.merge(user);
            return true;
        } catch (Exception e) {
            System.out.println("Cought while adding a user" + e);
            return false;
        }
    }
    
    public Boolean addComment(Comment com){
         EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Boolean b=false;
        em.getTransaction().begin();
        try {
            em.persist(com);
            em.getTransaction().commit();
           b=true;return b;
        } catch (Exception e) {
            e.printStackTrace(); 
            em.getTransaction().rollback();
            return b;
        }
    }

    public List getAllMovies() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Query q = em.createNamedQuery("Movie.findAll");
        List<Movie> movies = q.getResultList();
        return movies;
    }

    public List getAllUsers() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Query q = em.createNamedQuery("Userdata.findAll");
        List<Userdata> users = q.getResultList();
        return users;
    }
    
    public List getAllComments() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        Query q = em.createNamedQuery("Comment.findAll");
        List<Comment> comment = q.getResultList();
        return comment;
    }

    public void addMovie(JSONObject j) {
        Movie m = new Movie();
        String dateConvert;
        String pic;
        String score;
        try {
            m.setId(Integer.parseInt((String) j.get("id")));
            m.setMpaaRating(((String) j.get("mpaa_rating")));
            m.setTitle((String) j.get("title"));
            dateConvert = (String) ((JSONObject) j.get("release_dates")).get("theater");
            m.setReleaseDate(returnDate(dateConvert));
            m.setSynopsis((String) j.get("synopsis"));
            score = (String) j.get("critics_score");
            if (score != null) {
                m.setCriticScore(Integer.parseInt(score));
            } else {
                m.setCriticScore(-1);
            }
            score = (String) j.get("audience_score");
            if (score != null) {
                m.setAudienceScore(Integer.parseInt(score));
            } else {
                m.setAudienceScore(-1);
            }
            m.setRunTime(j.get("runtime").toString());
            m.setCastLink((String) ((JSONObject) j.get("links")).get("cast"));
            m.setClipsLink((String) ((JSONObject) j.get("links")).get("clips"));
            m.setReviewsLink((String) ((JSONObject) j.get("links")).get("reviews"));
            pic = (String) ((JSONObject) j.get("posters")).get("thumbnail");
            pic = formatImage(pic);
            if (pic == null) {
                pic = "http://alliance.education.nmsu.edu/files/2013/10/no-photo-available-icon.jpg";
            }
            m.setThumbnail(pic);
            this.persist(m);
        } catch (Exception e) {
            try {
                this.merge(m);
            } catch (Exception ex) {
                System.out.println("Caought while merging");
                ex.printStackTrace();
            }
        }
    }

    public Theater addTheater(String id, String name, String url, String phone, String address, String city, String state,
            String zip, String amenities, String boInfo, String agePolicy, String imageURL) {
        try {
            t.setId(id);
            System.out.println("in add " + id);
            t.setName(name);
            t.setUrl(url);
            t.setPhoneNumber(phone);
            t.setAddress(address);
            t.setCity(city);
            t.setState(state);
            t.setZip(Integer.parseInt(zip));
            t.setAmenities(amenities);
            t.setBoInfo(boInfo);
            t.setAgePolicy(agePolicy);
            t.setImgUrl(imageURL);
            this.persist(t);
            return t;
        } catch (Exception e) {
            System.out.println("Cought while persisitng theater object:");
            e.printStackTrace();
            return null;
        }
    }

    public void persist(Object object) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            //e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void merge(Object object) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("GinkoPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}
