/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.JoinFetch;

/**
 *
 * @author RAVI
 */
@Entity
@Table(name = "movie")
@NamedQueries({
    @NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m"),
    @NamedQuery(name = "Movie.findByReleaseDate", query = "SELECT m FROM Movie m WHERE m.releaseDate = :releaseDate"),
    @NamedQuery(name = "Movie.findById", query = "SELECT m FROM Movie m WHERE m.id = :id"),
    @NamedQuery(name = "Movie.findByMpaaRating", query = "SELECT m FROM Movie m WHERE m.mpaaRating = :mpaaRating"),
    @NamedQuery(name = "Movie.findByCriticScore", query = "SELECT m FROM Movie m WHERE m.criticScore = :criticScore"),
    @NamedQuery(name = "Movie.findByAudienceScore", query = "SELECT m FROM Movie m WHERE m.audienceScore = :audienceScore"),
    @NamedQuery(name = "Movie.findByThumbnail", query = "SELECT m FROM Movie m WHERE m.thumbnail = :thumbnail"),
    @NamedQuery(name = "Movie.findByRunTime", query = "SELECT m FROM Movie m WHERE m.runTime = :runTime"),
    @NamedQuery(name = "Movie.findByCastLink", query = "SELECT m FROM Movie m WHERE m.castLink = :castLink"),
    @NamedQuery(name = "Movie.findByClipsLink", query = "SELECT m FROM Movie m WHERE m.clipsLink = :clipsLink"),
    @NamedQuery(name = "Movie.findByReviewsLink", query = "SELECT m FROM Movie m WHERE m.reviewsLink = :reviewsLink"),
    @NamedQuery(name = "Movie.findByTitle", query = "SELECT m FROM Movie m WHERE m.title = :title")})
public class Movie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Lob
    @Column(name = "synopsis")
    private String synopsis;
    @Basic(optional = false)
    @Column(name = "release_date")
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "mpaa_rating")
    private String mpaaRating;
    @Column(name = "critic_score")
    private Integer criticScore;
    @Column(name = "audience_score")
    private Integer audienceScore;
    @Column(name = "thumbnail")
    private String thumbnail;
    @Column(name = "run_time")
    private String runTime;
    @Column(name = "cast_link")
    private String castLink;
    @Column(name = "clips_link")
    private String clipsLink;
    @Column(name = "reviews_link")
    private String reviewsLink;
    @Column(name = "title")
    private String title;
    @CascadeOnDelete
    @OneToMany(mappedBy = "movieId",cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    @JoinFetch
    private Collection<Comment> commentCollection;

    public Movie() {
    }

    public Movie(Integer id) {
        this.id = id;
    }

    public Movie(Integer id, Date releaseDate) {
        this.id = id;
        this.releaseDate = releaseDate;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMpaaRating() {
        return mpaaRating;
    }

    public void setMpaaRating(String mpaaRating) {
        this.mpaaRating = mpaaRating;
    }

    public Integer getCriticScore() {
        return criticScore;
    }

    public void setCriticScore(Integer criticScore) {
        this.criticScore = criticScore;
    }

    public Integer getAudienceScore() {
        return audienceScore;
    }

    public void setAudienceScore(Integer audienceScore) {
        this.audienceScore = audienceScore;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getCastLink() {
        return castLink;
    }

    public void setCastLink(String castLink) {
        this.castLink = castLink;
    }

    public String getClipsLink() {
        return clipsLink;
    }

    public void setClipsLink(String clipsLink) {
        this.clipsLink = clipsLink;
    }

    public String getReviewsLink() {
        return reviewsLink;
    }

    public void setReviewsLink(String reviewsLink) {
        this.reviewsLink = reviewsLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Collection<Comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<Comment> commentCollection) {
        this.commentCollection = commentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movie)) {
            return false;
        }
        Movie other = (Movie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Movie[ id=" + id + " ]";
    }
    
}
