<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Movie Page</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">

        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">

        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <!-- Page scripts -->
        <link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
    </head>
    <body>
        <jsp:useBean class="beans.Movie" id="curMovie" scope="session" ></jsp:useBean>
        <jsp:include page="/header.jsp"/>
        <div class="pg-opt">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h2>${curMovie.getTitle()}</h2>
                    </div>
                </div>
            </div>
        </div>

        <section class="slice bg-white">
            <div class="wp-section">
                <div class="container">
                    <div class="row ">  
                        <div class="col-md-3">
                            <img src="${curMovie.getThumbnail()}" width="200" height="100" class="img-responsive" alt="">
                            <br><p>Rate the movie: </p>
                            <div class="rw-ui-container"></div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-7">
                                    <h4 class="title">Run Time: ${curMovie.getRunTime()} minutes <br>
                                        <br>Rating: ${curMovie.getMpaaRating()}<br>
                                        <br>Release date: ${curMovie.getReleaseDate()}
                                    </h4>
                                </div>
                                <div class="col-md-5">
                                    <div class="social-media social-media-sm text-right">
                                        <a href="#" title="Facebook"><i class="fa fa-facebook facebook"></i></a>
                                        <a href="#" title="Google +"><i class="fa fa-google-plus google"></i></a>
                                        <a href="#" title="Twitter"><i class="fa fa-twitter twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                            <h3 class="title-lg" >Synopsis</h3>
                            <div style="width:850px;height:250px;line-height:1em;overflow:auto;padding:5px;"> 

                                <p >
                                    "${curMovie.getSynopsis()}"
                                </p>
                            </div>
                        </div> 
                                <div class="social-media social-media-sm text-right">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=http://localhost:8080/Ginko/MoviePage?movieID=${mov.getId()} target='_blank'" title="Facebook"><i class="fa fa-facebook facebook"></i></a>
                                        <a href="#" title="Google +"><i class="fa fa-google-plus google"></i></a>
                                        <a href="#" title="Twitter"><i class="fa fa-twitter twitter"></i></a>
                                    </div>
                        <div class="row">

                            <div class="col-md-12">
                                <!--<div class="product-info">-->
                                <div class="center-block">
                                    <iframe width="560" height="315" src="http://joshuanoll.com:6521/308stuff/getTrailers.php?name=${curMovie.title}" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div> </div>
                        <div class="col"> 
                            <div class="comment-list clearfix" >
                                <h2 class="comment-count" name="commentCollection">Comments:</h2>
                                <ol id="allComments">
                                    <c:if test="${curMovie.getCommentCollection().size()!=0}">
                                        <c:forEach items="${curMovie.getCommentCollection()}" var="com" >
                                            <li class="comment">
                                                <div class="comment-body boxed">
                                                    <div class="comment-avatar">
                                                        <div class="avatar"><img src="images/temp/fb-dp.jpg" alt=""></div>
                                                    </div>
                                                    <div class="comment-text">
                                                        <div class="comment-author clearfix">
                                                            <a href="#" class="link-author">${com.getUserId().getUsername()}</a>
                                                        </div>
                                                        <div class="comment-entry">
                                                            ${com.getContent()}
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </c:forEach>
                                    </c:if>
                                    <li id="comments" class="comment">
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="col">
                            <div class="section-title-wr">
                                <h3 class="section-title left"><span>Add A Comment</span></h3>
                            </div>

                            <!--<form class="form-light" role="form">-->
                            <p>
                                Improve this conversation by adding your comment :)
                            </p>
                            <div class="form-group form-group-lg">
                                <label class="" >Comment</label>
                                <!--<p id="labelChange">heyo</p>-->
                                <textarea id="addComment" class="form-control" rows="5" placeholder="Type you comment"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6">
                                    <button class="btn btn-lg btn-alt btn-icon btn-icon-right btn-comment pull-right" id="toBeClicked">
                                        <span>Send comment</span>
                                    </button>
                                </div>
                            </div>
                            <!--</form>-->

                        </div>
                    </div> 
                </div>
            </div>

        </section>



        <!-- Essentials -->
        <script src="js/modernizr.custom.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script src="js/jquery.metadata.js"></script>
        <script src="js/jquery.hoverup.js"></script>
        <script src="js/jquery.hoverdir.js"></script>
        <script src="js/jquery.stellar.js"></script>
        <!-- Boomerang mobile nav - Optional  -->
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>
        <!-- Forms -->
        <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script>
        <script src="assets/ui-kit/js/cusel.min.js"></script>
        <script src="assets/sky-forms/js/jquery.form.min.js"></script>
        <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
        <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
        <script src="assets/sky-forms/js/jquery.modal.js"></script>
        <!-- Assets -->
        <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
        <script src="assets/mixitup/jquery.mixitup.js"></script>
        <script src="assets/mixitup/jquery.mixitup.init.js"></script>
        <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script src="assets/waypoints/waypoints.min.js"></script>
        <script src="assets/milestone-counter/jquery.countTo.js"></script>
        <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
        <script src="assets/social-buttons/js/rrssb.min.js"></script>
        <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
        <script src="assets/owl-carousel/owl.carousel.js"></script>
        <script src="assets/bootstrap/js/tooltip.js"></script>
        <script src="assets/bootstrap/js/popover.js"></script>
        <!-- Sripts for individual pages, depending on what plug-ins are used -->
        <script src="assets/layerslider/js/greensock.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
        <!-- Initializing the slider -->
        <script>
            jQuery("#layerslider").layerSlider({
                pauseOnHover: true,
                autoPlayVideos: false,
                skinsPath: 'assets/layerslider/skins/',
                responsive: false,
                responsiveUnder: 1280,
                layersContainer: 1280,
                skin: 'borderlessdark3d',
                hoverPrevNext: true,
            });
        </script>
        <!-- Boomerang App JS -->
        <script src="js/wp.app.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <!-- Temp -- You can remove this once you started to work on your project -->
        <script src="js/jquery.cookie.js"></script>
        <script src="js/wp.switcher.js"></script>
        <script type="text/javascript" src="js/wp.ga.js"></script>
        <script type="text/javascript">(function (d, t, e, m) {

                // Async Rating-Widget initialization.
                window.RW_Async_Init = function () {

                    RW.init({
                        huid: "236054",
                        uid: "e13e9628ae5b0be369f2e623f0fa4071",
                        source: "website",
                        options: {
                            "size": "medium",
                            "style": "oxygen"
                        }
                    });
                    RW.render();
                };
                // Append Rating-Widget JavaScript library.
                var rw, s = d.getElementsByTagName(e)[0], id = "rw-js",
                        l = d.location, ck = "Y" + t.getFullYear() +
                        "M" + t.getMonth() + "D" + t.getDate(), p = l.protocol,
                        f = ((l.search.indexOf("DBG=") > -1) ? "" : ".min"),
                        a = ("https:" == p ? "secure." + m + "js/" : "js." + m);
                if (d.getElementById(id))
                    return;
                rw = d.createElement(e);
                rw.id = id;
                rw.async = true;
                rw.type = "text/javascript";
                rw.src = p + "//" + a + "external" + f + ".js?ck=" + ck;
                s.parentNode.insertBefore(rw, s);
            }(document, new Date(), "script", "rating-widget.com/"));</script>
        <script>
            $(document).ready(function () {
                $("#toBeClicked").click(function (event) {
                    //alert("Hello world!");
                    var queryString = $("#addComment").prop("value");
                    $.ajax({
                        url: "CommentControl",
                        data: {
                            addComment: queryString,
                            movie: "true"
                        },
                        success: function (responseText) {
                            $('#allComments').append(responseText);
                            $("#addComment").val("");
                            $("#toBeClicked").blur();
                        }
                    });
                });
            });
        </script>
    </body>
</html>
