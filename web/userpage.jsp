<%-- 
    Document   : userpage
    Created on : Apr 15, 2015, 6:35:30 PM
    Author     : Ritvik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>User Account Page</title>

        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> 
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen"> 

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">


        <!-- Favicon -->
        <link href="images/favicon.png" rel="icon" type="image/png">

        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">    
        <!--[if lt IE 9]>
            <link rel="stylesheet" href="assets/sky-forms/css/sky-forms-ie8.css">
        <![endif]-->

        <!-- Required JS -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>

        <!-- Page scripts -->

    </head>
    <body>
        <jsp:useBean id="userData" class="beans.Userdata" scope="session"></jsp:useBean>
            <!-- MODALS -->


            <jsp:include page="/header.jsp"/>
        <div class="pg-opt">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Welcome, ${userData.firstname}</h2>
                    </div>
                </div>
            </div>
        </div>

        <section class="slice bg-white">
            <div class="wp-section user-account">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="user-profile-img">
                                <img src="images/prv/team/team-agency-2.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-9">                     
                            <div class="tabs-framed">
                                <ul class="tabs clearfix">
                                    <li class="active"><a href="#tab-1" data-toggle="tab">About me</a></li>
                                    <li><a href="#tab-2" data-toggle="tab">Update Password</a></li>
                                    <li><a href="#tab-3" data-toggle="tab">Add Payment Method</a></li>
                                    <li><a href="#tab-4" data-toggle="tab">Edit Profile</a></li>
                                    <li><a href="#tab-5" data-toggle="tab">Update Theaters</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab-1">
                                        <div class="tab-body">
                                            <dl class="dl-horizontal style-2">
                                                <h3 class="title title-lg">Personal information</h3>
                                                <p class="mb-20"></p>
                                                <dt>Your name</dt>
                                                <dd>
                                                    <span class="pull-left">${userData.firstname} ${userData.lastname}</span>
                                                </dd>
                                                <hr>
                                                <dt>Email</dt>
                                                <dd>
                                                    <span class="pull-left">${userData.email}</span>
                                                </dd>
                                                <hr>
                                                <dt>Phone</dt>
                                                <dd>
                                                    <span class="pull-left">${userData.phone}</span>
                                                </dd>
                                                <hr>
                                                <dt>Address</dt>
                                                <dd>
                                                    <span class="pull-left">${userData.address} ${userData.city} ${userData.zip}</span>
                                                </dd>
                                                <hr>
                                                <dt>Other Information</dt>
                                                <dd>
                                                    <span class="pull-left">${userData.otherinfo}</span>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="tab-2">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <p class="mb-20"></p>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-6">
                                                        <div class="wp-block default user-form no-margin"> 
                                                            <div class="form-body">
                                                                <form action="http://localhost:8080/Ginko/PasswordUpdate" class="sky-form">                                     
                                                                    <fieldset>                  
                                                                        <section>
                                                                            <div class="form-group">
                                                                                <label class="label">Old Password</label>
                                                                                <label class="input">
                                                                                    <input type="password" name="oldpassword">
                                                                                </label>
                                                                            </div>     
                                                                        </section>
                                                                        <section>
                                                                            <div class="form-group">
                                                                                <label class="label">New Password</label>
                                                                                <label class="input">
                                                                                    <input type="password" name="newpassword">
                                                                                </label>
                                                                            </div>     
                                                                        </section> 
                                                                        <section>
                                                                            <div class="form-group">
                                                                                <label class="label">Confirm New Password</label>
                                                                                <label class="input">
                                                                                    <input type="password" name="confirmnewpassword">
                                                                                </label>
                                                                            </div>     
                                                                        </section> 
                                                                        <section>
                                                                            <button class="btn btn-base btn-icon btn-icon-right btn-sign-in pull-right" type="submit">
                                                                                <span>Change</span>
                                                                            </button>
                                                                        </section>
                                                                    </fieldset>  
                                                                </form>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-3">
                                        <div class="tab-body">
                                            <p class="mb-20"></p>
                                            <div class="wp-section">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="wp-block default user-form payment-form">    
                                                                <div class="form-header">
                                                                    <h2>Enter Details</h2>
                                                                </div>
                                                                <div class="form-body">
                                                                    <form action="" class="sky-form" novalidate="novalidate"> 
                                                                        <fieldset>

                                                                            <section>
                                                                                <div class="inline-group">
                                                                                    <label class="radio"><input type="radio" name="radio-inline" checked=""><i></i>Visa</label>
                                                                                    <label class="radio"><input type="radio" name="radio-inline"><i></i>MasterCard</label>
                                                                                </div>
                                                                            </section>                  
                                                                            <section>
                                                                                <label class="input">
                                                                                    <input type="text" name="name" placeholder="Name on card">
                                                                                </label>
                                                                            </section>
                                                                            <div class="row">
                                                                                <section class="col-md-10">
                                                                                    <label class="input state-success">
                                                                                        <input type="text" name="card" id="card" placeholder="Card number" class="valid">
                                                                                    </label>
                                                                                </section>
                                                                                <section class="col-md-2">
                                                                                    <label class="input">
                                                                                        <input type="text" name="cvv" id="cvv" placeholder="CVV2">
                                                                                    </label>
                                                                                    
                                                                                </section>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <label class="label">Expiration date</label>
                                                                                </div>
                                                                                <section class="col-md-5">
                                                                                    <label class="select">
                                                                                        <select name="month">
                                                                                            <option value="0" selected="" disabled="">Month</option>
                                                                                            <option value="1">January</option>
                                                                                            <option value="1">February</option>
                                                                                            <option value="3">March</option>
                                                                                            <option value="4">April</option>
                                                                                            <option value="5">May</option>
                                                                                            <option value="6">June</option>
                                                                                            <option value="7">July</option>
                                                                                            <option value="8">August</option>
                                                                                            <option value="9">September</option>
                                                                                            <option value="10">October</option>
                                                                                            <option value="11">November</option>
                                                                                            <option value="12">December</option>
                                                                                        </select>
                                                                                        <i></i>
                                                                                    </label>
                                                                                </section>
                                                                            </div>
                                                                            <section>
                                                                                <div class="col">
                                                                                    <button class="btn btn-alt btn-icon btn-icon-go btn-icon-go pull-right" type="submit">
                                                                                        <span>Add Method</span>
                                                                                    </button>
                                                                                </div>
                                                                            </section>
                                                                        </fieldset>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-4">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">Edit Profile</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-8">                   
                                                                <div class="wp-block default user-form no-margin">
                                                                    <div class="form-header">
                                                                        <h2>Edit Details</h2>
                                                                    </div>
                                                                    <div class="form-body">
                                                                        <form action="http://localhost:8080/Ginko/UpdateUser" id="frmRegister" class="sky-form">                                    
                                                                            <fieldset class="no-padding">           
                                                                                <section class=""> 
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-user"></i>
                                                                                                    <input type="text" name="username" value="${userData.getUsername()}" placeholder="Username">
                                                                                                    <b class="tooltip tooltip-bottom-right">Needed to enter the website</b>
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-envelope-o"></i>
                                                                                                        <input type="email" name="email" value="${userData.getEmail()}" placeholder="E-mail">
                                                                                                        <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div>   
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-lock"></i>
                                                                                                    <input type="password" name="password" value="${userData.getPassword()}"placeholder="Password">
                                                                                                    <b class="tooltip tooltip-bottom-right">Needed to enter the Web site</b>
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div>   
                                                                                </section>

                                                                                <section class="no-margin">
                                                                                    <div class="row">
                                                                                        <section class="col-xs-6">
                                                                                            <label class="input">
                                                                                                <i class="icon-append fa fa-user"></i>
                                                                                                <input type="text" name="fname" value="${userData.getFirstname()}" placeholder="First name">
                                                                                            </label>
                                                                                        </section>
                                                                                        <section class="col-xs-6">
                                                                                            <label class="input">
                                                                                                <i class="icon-append fa fa-user"></i>
                                                                                                <input type="text" name="lname" value="${userData.getLastname()}"placeholder="Last name">
                                                                                            </label>
                                                                                        </section>
                                                                                    </div> 
                                                                                    <div class="row">
                                                                                        <section class="col-xs-6">
                                                                                            <label class="input">
                                                                                                <i class="icon-append fa fa-phone"></i>
                                                                                                <input type="tel" name="phone" value="${userData.getPhone()}" placeholder="Phone">
                                                                                            </label>
                                                                                        </section>
                                                                                    </div>
                                                                                </section>
                                                                            </fieldset>  

                                                                            <fieldset>
                                                                                <div class="row">
                                                                                    <section class="col-xs-4">
                                                                                        <label class="input">
                                                                                            <input type="text" name="city"  value="${userData.getCity()}" placeholder="City">
                                                                                        </label>
                                                                                    </section>

                                                                                    <section class="col-xs-4">
                                                                                        <label class="input">
                                                                                            <input type="text" name="zip" value="${userData.getZip()}" placeholder="Post code">
                                                                                        </label>
                                                                                    </section>
                                                                                    <section class="col-xs-4">
                                                                                        <label class="input">
                                                                                            <input type="text" name="state" value="${userData.getState()}" placeholder="State">
                                                                                        </label>
                                                                                    </section>
                                                                                </div>

                                                                                <section>
                                                                                    <label for="file" class="input">
                                                                                        <input type="text" name="address" value="${userData.getAddress()}"placeholder="Address">
                                                                                    </label>
                                                                                </section>

                                                                                <section>
                                                                                    <label class="textarea">
                                                                                        <textarea rows="3" name="info"  placeholder="Additional info">${userData.getOtherinfo()}</textarea>
                                                                                    </label>
                                                                                </section>
                                                                                <section>
                                                                                    <div class="row">
                                                                                        <div class="col-md-8">
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <button class="btn btn-alt btn-icon btn-icon-right btn-icon-go pull-right" type="submit">
                                                                                                <span>Update Info</span>
                                                                                            </button>
                                                                                        </div>
                                                                                </section>
                                                                            </fieldset>
                                                                        </form>  
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="tab-pane fade" id="tab-5">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">your theaters</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <!--<div class="table-responsive">-->
                                                            <div>
                                                                <table class="table table-bordered table-hover table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Theater</th>
                                                                            <th>Location</th>
                                                                            <th>Delete</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="allTheaters">
                                                                        <tr>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            $(document).ready(function () {
                //alert("Hello world!");
                //var queryString = $("#addComment").prop("value");
                
                $.ajax({
                    url: "AllTheaters",
                    success: function (responseText) {
                        $('#allTheaters').append(responseText);
                    }
                });
            });
        </script>

        <!-- FOOTER -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="col">
                            <h4>Subscription</h4>
                            <p>Sign up if you would like to receive news.</p>
                            <form class="form-horizontal form-light">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Your email address...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-base" type="button">Go!</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="col">
                            <h4>About us</h4>
                            <p class="no-margin">
                                Ginko is a movie information system that makes it super easy for you to go to the movies, the moment you decide to. <br>
                                <br>
                                Team Ginkgo:<br>
                                Wenhao Lu<br>
                                Shan Liu<br>
                                Ritvik Handa<br>
                                Ravikiran Nageli<br>
                                <br><br>

                            </p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="col col-social-icons">
                            <h4>Follow us</h4>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-skype"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                            <a href="#"><i class="fa fa-flickr"></i></a>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="col">
                            <h4>Contact us</h4>
                            <ul>
                                <li>Computer Science Building, Stony Brook United States</li>
                                <li>Phone: 111-222-3333 </li>
                                <li>Email: <a href="mailto:hello@example.com" title="Email Us">contact@ginkgo.com</a></li>
                                <li>Skype: <a href="skype:my.business?call" title="Skype us">team_ginkgo</a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <hr>


            </div>
        </footer>
    </div>

    <!-- Essentials -->
    <script src="js/modernizr.custom.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="js/jquery.easing.js"></script>
    <script src="js/jquery.metadata.js"></script>
    <script src="js/jquery.hoverup.js"></script>
    <script src="js/jquery.hoverdir.js"></script>
    <script src="js/jquery.stellar.js"></script>

    <!-- Boomerang mobile nav - Optional  -->
    <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
    <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>

    <!-- Forms -->
    <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script> 
    <script src="assets/ui-kit/js/cusel.min.js"></script>
    <script src="assets/sky-forms/js/jquery.form.min.js"></script>
    <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
    <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
    <script src="assets/sky-forms/js/jquery.modal.js"></script>

    <!-- Assets -->
    <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
    <script src="assets/mixitup/jquery.mixitup.js"></script>
    <script src="assets/mixitup/jquery.mixitup.init.js"></script>
    <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script src="assets/waypoints/waypoints.min.js"></script>
    <script src="assets/milestone-counter/jquery.countTo.js"></script>
    <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
    <script src="assets/social-buttons/js/rrssb.min.js"></script>
    <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
    <script src="assets/owl-carousel/owl.carousel.js"></script>
    <script src="assets/bootstrap/js/tooltip.js"></script>
    <script src="assets/bootstrap/js/popover.js"></script>

    <!-- Sripts for individual pages, depending on what plug-ins are used -->

    <!-- Boomerang App JS -->
    <script src="js/wp.app.js"></script>
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- Temp -- You can remove this once you started to work on your project -->
    <script src="js/jquery.cookie.js"></script>
    <script src="js/wp.switcher.js"></script>
    <script type="text/javascript" src="js/wp.ga.js"></script>


</body>
</html>