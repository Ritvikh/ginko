/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import beans.Movie;
import controller.CrudOperator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author RAVI
 */
@WebServlet("/UpdateMovie/*")
public class UpdateMovie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            
            CrudOperator c = new CrudOperator();
            HttpSession ses=request.getSession();
            Movie m=(Movie) ses.getAttribute("curMovie");
            m.setTitle(request.getParameter("title"));
            m.setReleaseDate(c.returnDate(request.getParameter("release_date")));
            m.setMpaaRating(request.getParameter("mpaa_rating"));
            m.setThumbnail(request.getParameter("thumbnail"));
            m.setRunTime(request.getParameter("run_time"));
            m.setClipsLink(request.getParameter("clips_link"));
            m.setSynopsis(request.getParameter("info"));
            if (c.updateMovie(m)) {
                request.getRequestDispatcher("/admin.jsp").include(request, response);
            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Invalid Credentials');");
                out.println("location='admin.jsp';");
                out.println("</script>");
            }
        } catch (NumberFormatException | ServletException | IOException e) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Invalid Credentials');");
            out.println("location='admin.jsp';");
            out.println("</script>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
