package controller;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author RAVI
 */
import beans.Theater;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FandangoScraper {

    //public static final String API_KEY = "gs2spwmu9dt6uqnaxhsadxp6";
    //public static final String BASE_URL = "http://api.rottentomatoes.com/api/public/v1.0";
    Date curDate = new Date();
    Theater th = new Theater();

    CrudOperator cl = new CrudOperator();

    public void getTheaters(String near) {
        try {
            //Document getTimes = Jsoup.connect("https://tickets.fandango.com/Transaction/Ticketing/ticketboxoffice.aspx?row_count=86250465&mid=180391&tid=AALAT&intcmp=spotlight:hot+pursuit:showtime").get();
            Document doc = Jsoup.connect("http://www.fandango.com/"+near+"_movietimes").get();
            Elements theaterList = doc.select("#nearbyTheatersDropdown");
            for (Element theater : theaterList) {
                for (Element e : theater.select("option")) {
                    String s = e.val();
                    if (s.indexOf('_') > 0) {
                        String name = e.text();
                        System.out.println("Name "+e.text());
                        String url = s.substring(0, s.indexOf('?'));//theaterPage URL is stored in here.
                        String s1 = s.substring(s.indexOf('_'), s.indexOf('/', s.indexOf('_')));
                        StringBuilder sb = new StringBuilder(s1);
                        sb.deleteCharAt(0);
                        s1 = sb.toString();
                        System.out.println(" Url "+url);
                        Document showTimesDoc = Jsoup.connect(url).get();
                        Elements addressList = showTimesDoc.getElementsByClass("showtimes-theater-location");
                        String fullAddress = addressList.get(0).children().get(0).text();
                        String phoneNumber = addressList.get(0).children().get(2).text();
                        String address[] = fullAddress.split(",");
                        String street = address[0];
                        String city = address[1];
                        String stateZip[] = address[2].split(" ");
                        String state = stateZip[1];
                        state = state.replaceAll("\\s", "");
                        String zip = stateZip[2];
                        //System.out.println("address: " + street + " " + city + " " + state + " " + zip + " " + phoneNumber);
                        String amenities = showTimesDoc.select(".theater-amenities-all").text();
                       // System.out.println("Amen "+amenities);
                        String boInfo = showTimesDoc.select(".box-office-info").text();
                       // System.out.println("Box Info "+boInfo);
                        String agePolicy = showTimesDoc.select(".age-policy").text();
                        //System.out.println("Age Policy "+agePolicy);
                        String imageURL = showTimesDoc.select(".theater-logo img[src]").attr("src");
                        //System.out.println("Image "+imageURL);
                        //cl.addTheater(s1, name, url, phoneNumber, street, city, state, zip, amenities, boInfo, agePolicy, imageURL);
//                    String amenityList=amenities.get(0).children().get(1).text();
//                    for(Element theaterInf:theaterInfo ){
//                        System.out.println("theater name= "+theaterInfo.text());
//                    }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
            //return null;
        }
    }

    public void getTheaterTimings(String url) {

    }

//    public static JSONObject getMovieInfo(String movieId) {
//        try {
//            URL url = new URL(BASE_URL + "/movies/" + movieId + 
//                    ".json?apikey=" + API_KEY);
//            return (JSONObject) JSONValue.parse(new InputStreamReader(url.openStream()));
//        } 
//        catch (Exception ex) {
//            System.out.println(ex);
//            return null;
//        }
//    }
    public static void main(String a[]) {
//        FandangoScraper f = new FandangoScraper();
//        f.getTheaters("11790");
        TheatersNearYou th=new TheatersNearYou();
        List<Theater> thtr=th.getTheaters("11790");
        System.out.println("hi");
        for(Theater t:thtr){
            System.out.println("thatre"+t.getName());
  
        }
    }
}
