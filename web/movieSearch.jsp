<%-- 
    Document   : movieSearch
    Created on : May 13, 2015, 4:24:58 AM
    Author     : RAVI
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Search Results</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> 
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen"> 
        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">
        <!-- Favicon -->
        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">    
        <!-- Required JS -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
    </head>
    <body>
        <!-- MAIN CONTENT -->
        <jsp:include page="/header.jsp"/>
        <div class="pg-opt">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Search Results</h2>
                    </div>
                </div>
            </div>
        </div>


        <div class="tabs-framed">
            <ul class="tabs clearfix">
                <li class="active"><a href="#tab-1" data-toggle="tab">Movie Results</a></li>
                <li><a href="#tab-2" data-toggle="tab">Theater Results</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab-1">
                    <div class="tab-body">
                        <c:if test="${sessionScope.searchMovies!=null}">
                            <c:forEach items="${searchMovies}" var="mov" >
                                <section class="slice bg-white bb">
                                    <div class="wp-section">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="wp-block property list">
                                                                <div class="wp-block-title">
                                                                    <h3><a href="http://localhost:8080/Ginko/MoviePage?movieID=${mov.getId()}" >
                                                                            <c:out value="${mov.getTitle()}"/></a></h3>
                                                                </div>
                                                                <div class="wp-block-body">
                                                                    <div class="wp-block-img">
                                                                        <a href="#">
                                                                            <img src="${mov.getThumbnail()}" 
                                                                                 alt="" 
                                                                                 style="width:100%;max-width:200px;height:auto;">
                                                                        </a>
                                                                    </div>
                                                                    <div class="wp-block-content clearfix">
                                                                        <h4 class="content-title">Synopsis</h4>
                                                                        <p class="description"> ${mov.getSynopsis()}</p>
                                                                    </div>                                         
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section> 
                            </c:forEach>
                        </c:if>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-2">
                    <div class="tab-body" style="padding-bottom: 0;">
                        <p class="mb-20"></p>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <c:if test="${sessionScope.searchTheater!=null}">
                                    <c:forEach items="${searchTheater}" var="thtr" >
                                        <section class="slice bg-white bb">
                                            <div class="wp-section">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="wp-block property list">
                                                                        <div class="wp-block-title">
                                                                            <h3><a href="http://localhost:8080/Ginko/TheaterPage?theaterID=${thtr.getId()}&name=${thtr.getName()}&amp;near=uncheck" >
                                                                                    <c:out value="${thtr.getName()}"/></a></h3>
                                                                        </div>
                                                                        <div class="wp-block-body">
                                                                            <div class="wp-block-img">
                                                                                <a href="#">
                                                                                    <img src="${thtr.getImgUrl()}" 
                                                                                         alt="" 
                                                                                         style="width:100%;max-width:200px;height:auto;">
                                                                                </a>
                                                                            </div>
                                                                            <div class="wp-block-content clearfix">
                                                                                <h4 class="content-title">Address</h4>
                                                                                <p class="description"> ${thtr.getAddress()} ,${thtr.getCity()} ,${thtr.getState()} ${thtr.getZip()}</p>
                                                                            </div>                                         
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section> 
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Essentials -->
    <script src="js/modernizr.custom.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="js/jquery.easing.js"></script>
    <script src="js/jquery.metadata.js"></script>
    <script src="js/jquery.hoverup.js"></script>
    <script src="js/jquery.hoverdir.js"></script>
    <script src="js/jquery.stellar.js"></script>

    <!-- Boomerang mobile nav - Optional  -->
    <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
    <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>

    <!-- Forms -->
    <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script> 
    <script src="assets/ui-kit/js/cusel.min.js"></script>
    <script src="assets/sky-forms/js/jquery.form.min.js"></script>
    <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
    <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
    <script src="assets/sky-forms/js/jquery.modal.js"></script>

    <!-- Assets -->
    <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
    <script src="assets/mixitup/jquery.mixitup.js"></script>
    <script src="assets/mixitup/jquery.mixitup.init.js"></script>
    <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script src="assets/waypoints/waypoints.min.js"></script>
    <script src="assets/milestone-counter/jquery.countTo.js"></script>
    <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
    <script src="assets/social-buttons/js/rrssb.min.js"></script>
    <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
    <script src="assets/owl-carousel/owl.carousel.js"></script>
    <script src="assets/bootstrap/js/tooltip.js"></script>
    <script src="assets/bootstrap/js/popover.js"></script>

    <!-- Sripts for individual pages, depending on what plug-ins are used -->

    <!-- Boomerang App JS -->
    <script src="js/wp.app.js"></script>
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->

    <!-- Temp -- You can remove this once you started to work on your project -->
    <script src="js/jquery.cookie.js"></script>
    <script src="js/wp.switcher.js"></script>
    <script type="text/javascript" src="js/wp.ga.js"></script>


</body>
</html>