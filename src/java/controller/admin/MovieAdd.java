/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import beans.Movie;
import controller.CrudOperator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author RAVI
 */
@WebServlet(name = "MovieAdd", urlPatterns = {"/MovieAdd"})
public class MovieAdd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        CrudOperator cd=new CrudOperator();
        String name=request.getParameter("title");
        String releaseDate=request.getParameter("release_date");
        String mpaa_rating=request.getParameter("mpaa_rating");
        String imgurl=request.getParameter("thumbnail");
        String runtime=request.getParameter("run_time");
        String trailer=request.getParameter("clips_link");
        String synopsis=request.getParameter("info");
        int id = (int) Math.round(Math.random() * 100000);
        Movie m=new Movie();
        m.setId(id);
        m.setClipsLink(trailer);
        m.setReleaseDate(cd.returnDate(releaseDate));
        m.setMpaaRating(mpaa_rating);
        m.setThumbnail(imgurl);
        m.setSynopsis(synopsis);
        m.setTitle(name);
        m.setRunTime(runtime);
        if(cd.addMovie(m)){
            request.getRequestDispatcher("/admin.jsp").include(request, response);
        }else{
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Invalid Credentials');");
                out.println("location='admin.jsp';");
                out.println("</script>");
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
