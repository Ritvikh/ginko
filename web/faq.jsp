<%-- 
    Document   : faq
    Created on : May 20, 2015, 5:54:48 PM
    Author     : RAVI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Movie Information System</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">

        <!-- Favicon -->
        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <!-- Page scripts -->
        <link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
    </head>
    <body>
        <jsp:include page="/header.jsp"/>
        <p> <strong>FREQUENTLY ASKED QUESTIONS</strong></p>
        <p>&nbsp;</p>
        <p><strong>My account was deleted and I am unable to sign in. Why was my account deleted?</strong> </p>
        <p> It may be possible that we found you to be misusing our services. You can contact us for the exact reason behind the issue. We can sign you up again if possible.</p>
        <br>
        <p><strong>Why were my comments removed?</strong> </p>
        <p> It may be possible that we found your comments abusive or hurting someone's sentiments.</p>
        <br>
        <p><strong>I am not able to register for the website. What should I do?</strong> </p>
        <p> You can contact us at admin@ginko.com. We will register you and you can soon use our services</p>
        <br>
        <p><strong>How can I get a refund for my ticket?</strong> </p>
        <p> Contact us if find you feel you need a refund for your ticket. If your terms are well within the refund policy we will refund your money as soon as possible.</p>
        <br>
        <p><strong>How can I claim my money if the payment was made and I didn't get the ticket confirmed?</strong> </p>
        <p> We will refund you the entire amount. Please notify us as soon as possible.</p>
        <br>
        <br>
        <p>&nbsp;</p>






</body>
</html>

