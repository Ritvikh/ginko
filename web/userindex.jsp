<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>


    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Movie Information System</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">

        <!-- Favicon -->
        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script type="text/javascript"
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlJmHEtWBgLyhHsDpsbJzWY0SZO1EiNEg">
        </script>
        <!-- Page scripts -->
        <link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
    </head>
    <body>
        <jsp:include page="/header.jsp"/>
        <jsp:useBean id="userData" class="beans.Userdata" scope="session"></jsp:useBean>
          <section class="slice light-gray bb">
            <div class="wp-section">
                <div class="container">
                    <div class="section-title-wr">
                        <h3 class="section-title left"><span>Spotlight</span></h3>
                    </div>
                    <section class="slice bg-white animate-hover-slide">
                        <div class="wp-section work work-no-space g5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="sort-list-btn hidden-xs">
                                            <button type="button" class="btn btn-base filter active" data-filter="all"><i class="fa fa-th-large"></i> Show all</button>
                                            <button type="button" class="btn btn-white filter" data-filter="category_5 category_8">Action</button>
                                            <button type="button" class="btn btn-white filter" data-filter="category_9">Thriller</button>
                                            <button type="button" class="btn btn-white filter" data-filter="category_4 category_6 category_10">Romance</button>
                                            <button type="button" class="btn btn-white filter" data-filter="category_2">Documentary</button>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="btn-group pull-right hidden-md hidden-lg">
                                            <button type="button" class="btn btn-three">Filter films</button>
                                            <button type="button" class="btn btn-three dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu" id="ulFilterMenu">
                                                <li class="filter active" data-filter="all"><a>Show All</a></li>
                                                <li class="filter" data-filter="category_5 category_8"><a>Action</a></li>
                                                <li class="filter" data-filter="category_9"><a>Thriller</a></li>
                                                <li class="filter" data-filter="category_4 category_6 category_10"><a>Romance</a></li>
                                                <li class="filter" data-filter="category_2"><a>Documentary</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="ulSorList" style="margin:15px 15px;">
                                    <div class="mix category_1 mix_all" data-cat="1">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=770782126">
                                                <img src="http://content6.flixster.com/movie/11/18/95/11189520_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Child 44</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_2 mix_all" data-cat="2">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=770804151">
                                                <img src="http://content6.flixster.com/movie/11/19/07/11190716_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Lost River</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_3 mix_all" data-cat="3">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771270966">
                                                <img src="http://content6.flixster.com/movie/11/18/15/11181570_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Cinderella</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_4 mix_all" data-cat="4">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771311953">
                                                <img src="http://content6.flixster.com/movie/11/17/87/11178771_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Fifty Shades Of Gray</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_5 mix_all" data-cat="5">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771354922">
                                                <img src="http://content6.flixster.com/movie/11/18/14/11181482_ori.jpg" height="400" alt="" >
                                                <div class="caption base">
                                                    <h2 class="title">Furious 7</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_6 mix_all" data-cat="6">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771350902">
                                                <img src="http://content6.flixster.com/movie/11/18/92/11189234_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">The Longest Ride</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_7 mix_all" data-cat="7">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771319911">
                                                <img src="http://content6.flixster.com/movie/11/18/89/11188953_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Danny Collins</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_8 mix_all" data-cat="8">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771355806">
                                                <img src="http://content6.flixster.com/movie/11/18/90/11189059_ori.jpg" alt="" height="400">
                                                <div class="caption base">
                                                    <h2 class="title">Insurgent</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_9 mix_all" data-cat="9">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771321013">
                                                <img src="http://content6.flixster.com/movie/11/18/92/11189221_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Focus</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mix category_10 mix_all" data-cat="10">
                                        <div class="wp-block inverse">
                                            <a class="hov {shiftContent:30}" href="http://localhost:8080/Ginko/MoviePageOther?movieID=771315961">
                                                <img src="http://content6.flixster.com/movie/11/18/96/11189601_ori.jpg" alt="" height ="400">
                                                <div class="caption base">
                                                    <h2 class="title">Queen And Country</h2>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
        <section class="slice bg-white bb animate-hover-slide-3">
            <div class="wp-section">
                <div class="container">
                    <div class="section-title-wr">
                        <h3 class="section-title left"><span>Our team</span></h3>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="wp-block inverse">
                                <div class="figure">
                                    <img alt="" src="images/prv/team/team-corporate-1.jpg" class="img-responsive">
                                    <div class="figcaption">
                                        <ul class="social-icons text-right">
                                            <li class="text pull-left">More on:</li>
                                            <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h2>Ritvik Handa<small>Lead Programmer</small></h2>
                                <p>
                                </p>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="wp-block inverse">
                                <div class="figure">
                                    <img alt="" src="images/prv/team/team-corporate-2.jpg" class="img-responsive">
                                    <div class="figcaption">
                                        <ul class="social-icons text-right">
                                            <li class="text pull-left">More on:</li>
                                            <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h2>Ravikiran Nageli<small>Lead Designer</small></h2>
                                <p>
                                
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      
       
       
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="col">
                            <h4>Subscription</h4>
                            <p>Sign up if you would like to receive news.</p>
                            <form class="form-horizontal form-light" action="http://localhost:8080/Ginko/Subscribe">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="email" placeholder="Your email address...">
                                    <span class="input-group-btn">
                                        <input class="btn btn-base" value="Go"type="submit">
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col">
                            <h4>About us</h4>
                            <p class="no-margin">
                                Ginko is a movie information system that makes it super easy for you to go to the movies, the moment you decide to. <br>
                                <br>
                                Team Ginkgo:<br>
                                Ritvik Handa<br>
                                Ravikiran Nageli<br>
                                <br><br>

                            </p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="col col-social-icons">
                            <h4>Follow us</h4>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-skype"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                            <a href="#"><i class="fa fa-flickr"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col">
                            <h4 >Contact us</h4>
                            <p id="break">gsbrgbswerbg</p>
                            <ul>
                                <li>Computer Science Building, Stony Brook United States</li>
                                <li>Phone: 111-222-3333 </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script>var reqe;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (a) {
                    for (var t in a)
                    {
                        $('#general').append("<em>" + t + "</em> : " + a[t] + "<br/>");
                    }

                    if (a.address)
                    {
                        for (var t in a.address) {
                            $('#address').append('<em>' + t + '</em> : ' + a.address[t] + '<br/>');
                        }
                    }

                    if (a.coords)
                    {
                        $('#coords').append('<em>' + t + '</em> : ' + a.coords["latitude"] + '<br/>');
                        $('#coords').append('<em>' + t + '</em> : ' + a.coords["longitude"] + '<br/>');
                        //var latlng = new google.maps.LatLng(a.coords["latitude"], a.coords["longitude"]);
                        var latlng = new google.maps.LatLng(39.958996, -82.990787);
                        geocoder = new google.maps.Geocoder();

                        geocoder.geocode({'latLng': latlng}, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    for (j = 0; j < results[0].address_components.length; j++) {
                                        if (results[0].address_components[j].types[0] === 'postal_code'){
                                            var url="http://localhost:8080/Ginko/NearTheater?location="+results[0].address_components[j].short_name;
                                            reqe=new XMLHttpRequest();
                                            reqe.open("GET", url, true);
                                            reqe.send(null);
                                        }
                                            
//                                        document.getElementById("break").innerHTML = results[0].address_components[j].short_name;
//                                    alert("Zip Code: " + results[0].address_components[j].short_name);
                                    }
                                }
                            } else {
                                alert("Geocoder failed due to: " + status);
                            }
                        });
                    }
                });
            }
            else
            {
                alert('navigator.geolocation not supported.');
            }
        </script>
    <!-- Essentials -->
    <script src="js/modernizr.custom.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="js/jquery.easing.js"></script>
    <script src="js/jquery.metadata.js"></script>
    <script src="js/jquery.hoverup.js"></script>
    <script src="js/jquery.hoverdir.js"></script>
    <script src="js/jquery.stellar.js"></script>
    <!-- Boomerang mobile nav - Optional  -->
    <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
    <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>
    <!-- Forms -->
    <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script>
    <script src="assets/ui-kit/js/cusel.min.js"></script>
    <script src="assets/sky-forms/js/jquery.form.min.js"></script>
    <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
    <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
    <script src="assets/sky-forms/js/jquery.modal.js"></script>
    <!-- Assets -->
    <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
    <script src="assets/mixitup/jquery.mixitup.js"></script>
    <script src="assets/mixitup/jquery.mixitup.init.js"></script>
    <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script src="assets/waypoints/waypoints.min.js"></script>
    <script src="assets/milestone-counter/jquery.countTo.js"></script>
    <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
    <script src="assets/social-buttons/js/rrssb.min.js"></script>
    <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
    <script src="assets/owl-carousel/owl.carousel.js"></script>
    <script src="assets/bootstrap/js/tooltip.js"></script>
    <script src="assets/bootstrap/js/popover.js"></script>
    <!-- Sripts for individual pages, depending on what plug-ins are used -->
    <script src="assets/layerslider/js/greensock.js" type="text/javascript"></script>
    <script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
    <script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <!-- Initializing the slider -->
    <script>
        jQuery("#layerslider").layerSlider({
            pauseOnHover: true,
            autoPlayVideos: false,
            skinsPath: 'assets/layerslider/skins/',
            responsive: false,
            responsiveUnder: 1280,
            layersContainer: 1280,
            skin: 'borderlessdark3d',
            hoverPrevNext: true,
        });
    </script>
    <!-- Boomerang App JS -->
    <script src="js/wp.app.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <!-- Temp -- You can remove this once you started to work on your project -->
    <script src="js/jquery.cookie.js"></script>
    <script src="js/wp.switcher.js"></script>
    <script type="text/javascript" src="js/wp.ga.js"></script>
</body>
</html>