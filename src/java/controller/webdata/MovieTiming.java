/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.webdata;

import beans.Movie;
import beans.TheaterTiming;
import controller.CrudOperator;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author RAVI
 */
public class MovieTiming {

    List<TheaterTiming> time = new ArrayList<>();
    CrudOperator cd = new CrudOperator();
    List<Movie> m;

    public List getTheaters(String url,String dat) {
        try {String date=dat;
            if(date==null){
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
            Calendar cal=Calendar.getInstance();
            date = simpleDateFormat1.format(cal.getTime());
            }
           Document doc = Jsoup.connect("http://www.fandango.com/"+url + "/theaterpage?date="+date).get();
            for (Element e : doc.select(".showtimes-movie-container")) {
                String movie = e.select("h3").text();
                m = cd.getMovies(movie);
                if (m.isEmpty()) { //or null?
                    continue;
                }
                String allTimes = null;
                TheaterTiming tm = new TheaterTiming();
                tm.setMovie(m.get(0));
                List<String> times = new ArrayList<>();
                for (Element timing : e.select(".showtimes-times")) {
                    allTimes = timing.select(".btn-showtimes").text();
                    if (allTimes == null) {
                        allTimes = timing.select(".non-ticketing").text();
                    }
                    //System.out.println("Time "+time.select(".timeInfo").text());
                }if(allTimes == null){
                    continue;
                }
                if (allTimes.contains(" ")) {
                    String temp[] = allTimes.split(" ");
                    for (int i = 0; i < temp.length; i++) {
                        times.add(temp[i]);
                    }
                }else if(allTimes.length()>3){
                    times.add(allTimes);
                }

                tm.setTimes(times);
                time.add(tm);
                //System.out.println("MOvie: " + movie);
            }
        } catch (IOException ex) {
            System.out.println("Caought without executing anything" + ex);
            //return null;
        }return time;
    }
}
