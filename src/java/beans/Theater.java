/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author RAVI
 */
@Entity
@Table(name = "theater")
@NamedQueries({
    @NamedQuery(name = "Theater.findAll", query = "SELECT t FROM Theater t"),
    @NamedQuery(name = "Theater.findById", query = "SELECT t FROM Theater t WHERE t.id = :id"),
    @NamedQuery(name = "Theater.findByName", query = "SELECT t FROM Theater t WHERE t.name = :name"),
    @NamedQuery(name = "Theater.findByUrl", query = "SELECT t FROM Theater t WHERE t.url = :url"),
    @NamedQuery(name = "Theater.findByPhoneNumber", query = "SELECT t FROM Theater t WHERE t.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "Theater.findByAddress", query = "SELECT t FROM Theater t WHERE t.address = :address"),
    @NamedQuery(name = "Theater.findByState", query = "SELECT t FROM Theater t WHERE t.state = :state"),
    @NamedQuery(name = "Theater.findByZip", query = "SELECT t FROM Theater t WHERE t.zip = :zip"),
    @NamedQuery(name = "Theater.findByAmenities", query = "SELECT t FROM Theater t WHERE t.amenities = :amenities"),
    @NamedQuery(name = "Theater.findByImgUrl", query = "SELECT t FROM Theater t WHERE t.imgUrl = :imgUrl"),
    @NamedQuery(name = "Theater.findByCity", query = "SELECT t FROM Theater t WHERE t.city = :city")})
public class Theater implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "url")
    private String url;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "address")
    private String address;
    @Column(name = "state")
    private String state;
    @Column(name = "zip")
    private Integer zip;
    @Column(name = "amenities")
    private String amenities;
    @Lob
    @Column(name = "bo_info")
    private String boInfo;
    @Lob
    @Column(name = "age_policy")
    private String agePolicy;
    @Column(name = "img_url")
    private String imgUrl;
    @Column(name = "city")
    private String city;
    @OneToMany(mappedBy = "theaterId")
    private Collection<Comment> commentCollection;

    public Theater() {
    }

    public Theater(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public String getBoInfo() {
        return boInfo;
    }

    public void setBoInfo(String boInfo) {
        this.boInfo = boInfo;
    }

    public String getAgePolicy() {
        return agePolicy;
    }

    public void setAgePolicy(String agePolicy) {
        this.agePolicy = agePolicy;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Collection<Comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<Comment> commentCollection) {
        this.commentCollection = commentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Theater)) {
            return false;
        }
        Theater other = (Theater) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Theater[ id=" + id + " ]";
    }
    
}
