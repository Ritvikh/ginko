<%-- 
    Document   : success
    Created on : Apr 17, 2015, 6:40:35 AM
    Author     : Ritvik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Success!</title>
    </head>
    <body>
        <h2>Added to the database!       
        <% response.setHeader("Refresh", "3;url=admin.jsp"); %>
        </h2>
    </body>
</html>
