<%-- 
    Document   : payment
    Created on : May 18, 2015, 9:57:12 AM
    Author     : RAVI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Payment Page</title>
        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css"> 

        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">

        <!-- Favicon -->
        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms-ie8.css">
        <![endif]-->
        <!-- Required JS -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <!-- Page scripts -->
        <link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
    </head>
    <body>
        <!-- MAIN CONTENT -->
         <jsp:include page="/header.jsp"/>
        <section class="slice slice-lg bg-image" style="background-image:url(images/backgrounds/full-bg-1.jpg);">
            <div class="wp-section">
                <div class="container">
                    <div class="row"></div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="wp-block default user-form payment-form">    
                                <div class="form-header">
                                    <h2>Buy Tickets</h2>
                                </div>
                                <div class="form-body">
                                    <form action="http://localhost:8080/Ginko/Payment" class="sky-form" novalidate="novalidate"> 
                                        <div class="row">
                                            <label class="title-md">Movie: ${payMovie}</label>
                                        </div>
                                        <div class="row">
                                            <label class="title-md">Theater: ${curTheater.getName()}</label>
                                        </div>
                                        <div class="row">
                                            <label class="title-md">Timing: ${payTime}</label>
                                        </div>
                                        <fieldset>

                                            <div class="row">
                                                <section class="col-md-5">
                                                    <label class="select">
                                                        <select name="quantity" id="seats" onblur="updateAmount()">
                                                            <option value="0" selected="" disabled="">Select Quantity</option>
                                                            <option value="1">1</option>
                                                            <option value="1">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </section> 
                                                <p> Total Amount: $<span id="amount" >0</span> </p>
                                                <input type="hidden" name="totalamount" id="hid"/>
                                            </div>
                                            <section>
                                                <div class="inline-group">
                                                    <label class="radio"><input type="radio" name="radio-inline" checked=""><i></i>Visa</label>
                                                    <label class="radio"><input type="radio" name="radio-inline"><i></i>MasterCard</label>
                                                </div>
                                            </section>                  
                                            <section>
                                                <label class="input">
                                                    <input type="text" name="name" placeholder="Name on card">
                                                </label>
                                            </section>
                                            <div class="row">
                                                <section class="col-md-10">
                                                    <label class="input state-success">
                                                        <input type="text" name="card" id="card" placeholder="Card number" class="valid">
                                                    </label>
                                                </section>
                                                <section class="col-md-2">
                                                    <label class="input">
                                                        <input type="text" name="cvv" id="cvv" placeholder="CVV2">
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="label">Expiration date</label>
                                                </div>
                                                <section class="col-md-5" >
                                                    <label class="select" >
                                                        <select name="month" >
                                                            <option value="0" selected="" disabled="">Month</option>
                                                            <option value="1">January</option>
                                                            <option value="1">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </section>
                                            </div>
                                            <section>
                                                <div class="col">
                                                    <button class="btn btn-alt btn-icon btn-icon-go btn-icon-go pull-right" type="submit">
                                                        <span>Make Payment</span>
                                                    </button>
                                                </div>
                                            </section>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Essentials -->
        <script>
           function updateAmount(){
                var index=document.getElementById("seats");
                var amount=document.getElementById("amount");
                var hide=document.getElementById("hid");
                amount.innerHTML=index.selectedIndex*18;
                hide.innerHTML=index.selectedIndex*18;
                
            }
        </script>
        <script src="js/modernizr.custom.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script src="js/jquery.metadata.js"></script>
        <script src="js/jquery.hoverup.js"></script>
        <script src="js/jquery.hoverdir.js"></script>
        <script src="js/jquery.stellar.js"></script>
        <!-- Boomerang mobile nav - Optional  -->
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>
        <!-- Forms -->
        <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script>
        <script src="assets/ui-kit/js/cusel.min.js"></script>
        <script src="assets/sky-forms/js/jquery.form.min.js"></script>
        <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
        <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
        <script src="assets/sky-forms/js/jquery.modal.js"></script>
        <!-- Assets -->
        <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
        <script src="assets/mixitup/jquery.mixitup.js"></script>
        <script src="assets/mixitup/jquery.mixitup.init.js"></script>
        <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script src="assets/waypoints/waypoints.min.js"></script>
        <script src="assets/milestone-counter/jquery.countTo.js"></script>
        <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
        <script src="assets/social-buttons/js/rrssb.min.js"></script>
        <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
        <script src="assets/owl-carousel/owl.carousel.js"></script>
        <script src="assets/bootstrap/js/tooltip.js"></script>
        <script src="assets/bootstrap/js/popover.js"></script>
        <!-- Sripts for individual pages, depending on what plug-ins are used -->
        <script src="assets/layerslider/js/greensock.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
        <script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
        <!-- Initializing the slider -->
        <script>
            jQuery("#layerslider").layerSlider({
                pauseOnHover: true,
                autoPlayVideos: false,
                skinsPath: 'assets/layerslider/skins/',
                responsive: false,
                responsiveUnder: 1280,
                layersContainer: 1280,
                skin: 'borderlessdark3d',
                hoverPrevNext: true,
            });
        </script>
        <!-- Boomerang App JS -->
        <script src="js/wp.app.js"></script>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <!-- Temp -- You can remove this once you started to work on your project -->
        <script src="js/jquery.cookie.js"></script>
        <script src="js/wp.switcher.js"></script>
        <script type="text/javascript" src="js/wp.ga.js"></script>
    </body>
</html>