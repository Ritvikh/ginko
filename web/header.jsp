<%-- 
    Document   : header
    Created on : May 13, 2015, 7:58:12 AM
    Author     : RAVI
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="divHeaderWrapper">
            <header class="header-standard-2">
                <!-- MAIN NAV -->
                <div class="navbar navbar-wp navbar-arrow mega-nav" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle navbar-toggle-aside-menu">
                                <i class="fa fa-outdent icon-custom"></i>
                            </button>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <i class="fa fa-bars icon-custom"></i>
                            </button>
                            <a class="navbar-brand" href="userindex.jsp" title="Team Ginko for the win!">
                                <img src="images/ginko-logo.png" alt="Team Ginko for the win!">
                            </a>
                        </div>

                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="hidden-md hidden-lg">
                                    <div class="bg-light-gray">
                                        <form class="form-horizontal form-light p-15" role="form">
                                            <div class="input-group input-group-lg">
                                                <input type="text" class="form-control" placeholder="I want to find ...">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-white" type="button">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li class="dropdown dropdown-meganav mega-dropdown-fluid">
                                <li class="dropdown dropdown-meganav mega-dropdown-fluid">
                                    <a href="helppage.jsp" class="button">Help Page</a>
                                </li>
                                <li class="dropdown dropdown-meganav mega-dropdown-fluid">
                                    <a href="userindex.jsp" class="button">Home</a>
                                </li>
                                <%--<c:if test="${empty sessionScope.userData}">--%>
                                <c:choose>
                                    <c:when test="${userType=='user'}">
                                        <%--<c:if test="${userType=='user'}">--%>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                Welcome ${userData.username}
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="userpage.jsp">Profile Page</a></li>
                                                <li><a href="http://localhost:8080/Ginko/LogOut">Log out</a></li>
                                            </ul>
                                        </li>
                                        <%--</c:if>--%>
                                    </c:when> 
                                    
                                    <c:when test="${userType=='admin'}">
                                        <%--<c:if test="${userType=='admin'}">--%>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, Administrator</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="admin.jsp">Admin Page</a></li>
                                                <li><a href="http://localhost:8080/Ginko/LogOut">Log Out</a></li>
                                            </ul>
                                        </li>
                                        <%--</c:if>--%>
                                    </c:when>
                                        <c:otherwise>
                                        <li class="dropdown">
                                            <a href="sign in page.html" class="button">Sign In</a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="sign up page.html" class="button">Sign Up</a>
                                        </li>
                                        <%--</c:if>--%>
                                        </c:otherwise> 
                                </c:choose>
                                        <li class="dropdown dropdown-meganav mega-dropdown-fluid">
                                    <a href="faq.jsp" class="button">FAQ</a>
                                </li>
                                <li class="dropdown dropdown-meganav mega-dropdown-fluid">
                                    <a href="nearTheater.jsp" class="button">Theaters Near</a>
                                </li>

                                    <li class="dropdown dropdown-aux animate-click" data-animate-in="animated bounceInUp" data-animate-out="animated fadeOutDown" style="z-index:500;">
                                        <a href="#" class="dropdown-form-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-user animate-wr">
                                            <li id="dropdownForm">
                                                <div class="dropdown-form">
                                                    <form action="http://localhost:8080/Ginko/MovieSearch" method="post" class="form-horizontal form-light p-15" >
                                                        <div class="input-group">
                                                            <input type="text" name="searchResult" class="form-control" placeholder="Movies theaters or zip">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-base" type="submit">Go</button>
                                                            </span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </header>        
            </div>
        </body>
    </html>
