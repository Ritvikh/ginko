<%--<%@page import="java.awt.SystemColor.window"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<html>  
    <head>
        <title>Add Movie</title>
    </head>

    <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                       url="jdbc:mysql://mysql2.cs.stonybrook.edu:3306/ginkgo"
                       user="ginkgo"  password="changeit"/>

    <c:catch var ="catchException">
        <sql:update dataSource="${snapshot}" var="result">
            INSERT INTO moviedata VALUES ("${param.moviename}","${param.director}","${param.photolink}",
            "${param.embedURL}","${param.screenURL}","${param.screenURL2}","${param.screenURL3}","${param.screenURL4}","${param.info}");
        </sql:update>
    </c:catch>
    <body>
        <c:if test = "${catchException != null}">
            <h1> This movie has been added already! </h1></br>
            <p> Please enter a new movie name to add and try again! </p>
            <input value="go to previous page" type="button" onclick="javascript:history.back()">
<!--    <p>The exception is : ${catchException} <br />
    There is an exception: ${catchException.message}</p>-->
        </c:if>
        <c:if test = "${catchException == null}">                
            <c:redirect url="success.jsp"></c:redirect>
        </c:if>
    </body>
</html>

    
    

