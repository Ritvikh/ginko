<%-- 
    Document   : admin
    Created on : Apr 17, 2015, 6:13:03 AM
    Author     : Ritvik
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow">
        <title>Admin Page</title>

        <!-- Essential styles -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> 
        <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen"> 

        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">
        <!-- Favicon -->
        <link href="images/favicon.png" rel="icon" type="image/png">
        <!-- Assets -->
        <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">    
        <!-- Required JS -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.min.js"></script>

    </head>
    <body>
        <jsp:include page="/header.jsp"/>
        <jsp:useBean id="userData" class="beans.Userdata" scope="session"></jsp:useBean>
            <!-- MAIN CONTENT -->
            <section class="slice bg-white">
                <div class="wp-section user-account">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="user-profile-img">
                                    <img src="images/prv/team/team-agency-3.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">                     
                                <div class="tabs-framed">
                                    <ul class="tabs clearfix">
                                        <li class="active"><a href="#tab-1" data-toggle="tab">About me</a></li>
                                        <li><a href="#tab-2" data-toggle="tab">Movie Data</a></li>
                                        <li><a href="#tab-3" data-toggle="tab">User Data</a></li>
                                        <li><a href="#tab-4" data-toggle="tab">Add Movie</a></li>
                                        <li><a href="#tab-5" data-toggle="tab">Add User</a></li>
                                        <li><a href="#tab-6" data-toggle="tab">Comments</a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab-1">
                                            <div class="tab-body">
                                                <dl class="dl-horizontal style-2">
                                                    <h3 class="title title-lg">Personal information</h3>
                                                    <dt>Name</dt>
                                                    <dd>
                                                        <span class="pull-left">${userData.getFirstname()} ${userData.getLastname()}</span>
                                                </dd>
                                                <dt><br>Email</dt>
                                                <dd>
                                                    <span class="pull-left"><br>${userData.getEmail()}</span>
                                                </dd>
                                                <dt><br>Phone</dt>
                                                <dd>
                                                    <span class="pull-left"><br>${userData.getPhone()}</span>
                                                </dd>
                                                <dt><br>Address</dt>
                                                <dd>
                                                    <span class="pull-left"><br>${userData.getAddress()} ${userData.getCity()} ${userData.getZip()}</span>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-2">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">Movie Data</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-10">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-hover table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>ID</th>
                                                                            <th>Title</th>
                                                                            <th>Update</th>
                                                                            <th>Delete</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="allMovies">
                                                                        <tr>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-3">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">User Data</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <!--<div class="table-responsive">-->
                                                            <div>
                                                                <table class="table table-bordered table-hover table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Email</th>
                                                                            <th>Name</th>
                                                                            <th>Update</th>
                                                                            <th>Delete</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="allUsers">
                                                                        <tr>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-4">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">Add Movie</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-8">                   
                                                                <div class="wp-block default user-form no-margin">
                                                                    <div class="form-header">
                                                                        <h2>Enter Movie Details</h2>
                                                                    </div>
                                                                    <div class="form-body">
                                                                        <form action="http://localhost:8080/Ginko/MovieAdd" id="frmRegister" class="sky-form">                                    
                                                                            <fieldset class="no-padding">           
                                                                                <section class=""> 
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-user"></i>
                                                                                                    <input type="text" name="title" placeholder="Movie Name">
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-plus-circle"></i>
                                                                                                        <input type="text" name="release_date" placeholder="Release Date (yyyy-mm-dd)">
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div>   
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-plus-circle"></i>
                                                                                                    <input type="text" name="mpaa_rating" placeholder="Rating">
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-plus-circle"></i>
                                                                                                        <input type="text" name="thumbnail" placeholder="Image URL">
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div> 
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-plus-circle"></i>
                                                                                                    <input type="text" name="run_time" placeholder="Run Time">
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-plus-circle"></i>
                                                                                                        <input type="text" name="clips_link" placeholder="Trailer URL">
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div> 
                                                                                </section>
                                                                            </fieldset>  
                                                                            <fieldset>
                                                                                <section>
                                                                                    <label class="textarea">
                                                                                        <textarea rows="4" name="info" placeholder="Synopsis"></textarea>
                                                                                    </label>
                                                                                </section>
                                                                                <section>
                                                                                    <div class="col">
                                                                                        <button class="btn btn-alt btn-icon btn-icon-right btn-icon-go pull-right" type="submit">
                                                                                            <span>Add Movie</span>
                                                                                        </button>
                                                                                    </div>
                                                                                </section>
                                                                            </fieldset>
                                                                        </form>  
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-5">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">Add User</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-8">                   
                                                                <div class="wp-block default user-form no-margin">
                                                                    <div class="form-header">
                                                                        <h2>Enter User Details</h2>
                                                                    </div>
                                                                    <div class="form-body">
                                                                        <form action="http://localhost:8080/Ginko/UserAdd" id="frmRegister1" class="sky-form">                                    
                                                                            <fieldset class="no-padding">           
                                                                                <section class=""> 
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-user"></i>
                                                                                                    <input type="text" name="username" placeholder="User Name">
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-plus-circle"></i>
                                                                                                        <input type="email" name="email" placeholder="Email">
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div>   
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-plus-circle"></i>
                                                                                                    <input type="password" name="password" placeholder="Password">
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-plus-circle"></i>
                                                                                                        <input type="text" name="fname" placeholder="First Name">
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div> 
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <label class="input">
                                                                                                    <i class="icon-append fa fa-plus-circle"></i>
                                                                                                    <input type="text" name="lname" placeholder="Last Name">
                                                                                                </label>
                                                                                            </div>               
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <div class="form-group">
                                                                                                <div class="form-group">
                                                                                                    <label class="input">
                                                                                                        <i class="icon-append fa fa-plus-circle"></i>
                                                                                                        <input type="text" name="phone" placeholder="Phone Number">
                                                                                                    </label>
                                                                                                </div>  
                                                                                            </div>               
                                                                                        </div>
                                                                                    </div> 
                                                                                    <div class="row">
                                                                                        <section class="col-xs-4">
                                                                                            <label class="input">
                                                                                                <input type="text" name="city" placeholder="City">
                                                                                            </label>
                                                                                        </section>
                                                                                        <section class="col-xs-3">
                                                                                            <label class="input">
                                                                                                <input type="text" name="zip" placeholder="Post code">
                                                                                            </label>
                                                                                        </section>
                                                                                        <section class="col-xs-4">
                                                                                            <label class="input">
                                                                                                <input type="text" name="state" placeholder="State">
                                                                                            </label>
                                                                                        </section>
                                                                                    </div>
                                                                                    <section>
                                                                                        <label for="file" class="input">
                                                                                            <input type="text" name="address" placeholder="Address">
                                                                                        </label>
                                                                                    </section>
                                                                                </section>
                                                                            </fieldset>  
                                                                            <fieldset>
                                                                                <section>
                                                                                    <label class="textarea">
                                                                                        <textarea rows="4" name="info" placeholder="Other Info"></textarea>
                                                                                    </label>
                                                                                </section>
                                                                                <section>
                                                                                    <div class="col">
                                                                                        <button class="btn btn-alt btn-icon btn-icon-right btn-icon-go pull-right" type="submit">
                                                                                            <span>Add User</span>
                                                                                        </button>
                                                                                    </div>
                                                                                </section>
                                                                            </fieldset>
                                                                        </form>  
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-6">
                                        <div class="tab-body" style="padding-bottom: 0;">
                                            <h3 class="title title-lg">Delete Comments</h3>
                                            <p class="mb-20"></p>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <!--<div class="table-responsive">-->
                                                            <div>
                                                                <table class="table table-bordered table-hover table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Email</th>
                                                                            <th>Movie</th>
                                                                            <th>Comment</th>
                                                                            <th>Delete</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="allComments">
                                                                        <tr>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script>
            $(document).ready(function () {
                //alert("Hello world!");
                //var queryString = $("#addComment").prop("value");
                $.ajax({
                    url: "AllMovies",
                    success: function (responseText) {
                        $('#allMovies').append(responseText);
                    }
                });
                $.ajax({
                    url: "AllUsers",
                    success: function (responseText) {
                        $('#allUsers').append(responseText);
                    }
                });
                $.ajax({
                    url: "AllComments",
                    success: function (responseText) {
                        $('#allComments').append(responseText);
                    }
                });
            });
        </script>

        <!-- Essentials -->
        <script src="js/modernizr.custom.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script src="js/jquery.metadata.js"></script>
        <script src="js/jquery.hoverup.js"></script>
        <script src="js/jquery.hoverdir.js"></script>
        <script src="js/jquery.stellar.js"></script>

        <!-- Boomerang mobile nav - Optional  -->
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
        <script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>

        <!-- Forms -->
        <script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script> 
        <script src="assets/ui-kit/js/cusel.min.js"></script>
        <script src="assets/sky-forms/js/jquery.form.min.js"></script>
        <script src="assets/sky-forms/js/jquery.validate.min.js"></script>
        <script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
        <script src="assets/sky-forms/js/jquery.modal.js"></script>

        <!-- Assets -->
        <script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
        <script src="assets/mixitup/jquery.mixitup.js"></script>
        <script src="assets/mixitup/jquery.mixitup.init.js"></script>
        <script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script src="assets/waypoints/waypoints.min.js"></script>
        <script src="assets/milestone-counter/jquery.countTo.js"></script>
        <script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
        <script src="assets/social-buttons/js/rrssb.min.js"></script>
        <script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
        <script src="assets/owl-carousel/owl.carousel.js"></script>
        <script src="assets/bootstrap/js/tooltip.js"></script>
        <script src="assets/bootstrap/js/popover.js"></script>

        <!-- Sripts for individual pages, depending on what plug-ins are used -->

        <!-- Boomerang App JS -->
        <script src="js/wp.app.js"></script>
        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <!-- Temp -- You can remove this once you started to work on your project -->
        <script src="js/jquery.cookie.js"></script>
        <script src="js/wp.switcher.js"></script>
        <script type="text/javascript" src="js/wp.ga.js"></script>

        <script src="../bower_components/jquery/dist/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="bower_components/raphael/raphael-min.js"></script>
        <script src="bower_components/morrisjs/morris.min.js"></script>
        <script src="js/morris-data.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>
    </body>
</html>